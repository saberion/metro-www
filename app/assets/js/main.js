$('body').removeClass('no-js');
$(document).ready(function () {
	$('.banner-slider .owl-carousel').owlCarousel({
		items: 1
	});

	if ($('.s-sectors__item').length) {

		var update = function(){
			$('.s-sectors__item').each(function () {
				$(this).find('.s-sectors__item-info').height($(this).find('.s-sectors__item-head').height() + 15);
			});
		};

		update();

		$(window).on('resize', function () {
			update();
		});

		$('.s-sectors__item').each(function () {

			$(this).hover(function (e) {
				var dir = getDirection(e, this);
				// console.log(dir);
				$(this).find('.s-sectors__item-info').height($(this).find('.s-sectors__item-desc').height() + $(this).find('.s-sectors__item-head').height() + 15);
				addClass(e, this, 'in');
			}, function (e) {
				var dir = getDirection(e, this);
				// console.log('out ', dir);
				addClass(e, this, 'out');
				$(this).find('.s-sectors__item-info').height($(this).find('.s-sectors__item-head').height() + 15);
			});
		});

		var getDirection = function (ev, obj) {
			var w = obj.offsetWidth,
				h = obj.offsetHeight,
				x = (ev.pageX - obj.offsetLeft - (w / 2) * (w > h ? (h / w) : 1)),
				y = (ev.pageY - obj.offsetTop - $('.s-sectors').offset().top - (h / 2) * (h > w ? (w / h) : 1)),
				d = Math.round( Math.atan2(y, x) / 1.57079633 + 5 ) % 4;

			console.log(w, h , x , y, obj.offsetTop);

			return d;
		};

		var addClass = function ( ev, obj, state ) {
			var direction = getDirection( ev, obj ),
				class_suffix = "";

			obj.className = "s-sectors__item ";

			switch ( direction ) {
				case 0 : class_suffix = '-top';    break;
				case 1 : class_suffix = '-right';  break;
				case 2 : class_suffix = '-bottom'; break;
				case 3 : class_suffix = '-left';   break;
			}

			obj.classList.add( state + class_suffix );
		};
	}

	if($('.s-our-brands__slider .owl-carousel').length){

		var options = {
			items: items,
			loop: true,
			autoplay: 6000
		};

		var getItems = function (){
			var items = Math.round(($('.container-fluid').innerWidth()) / 150);
			return items;
		};

		var items = getItems();

		$(window).resize(function () {
			var items = getItems();
			if(options.items !== items){
				options.items = items;
				$('.s-our-brands__slider .owl-carousel').owlCarousel('destroy');
				$('.s-our-brands__slider .owl-carousel').owlCarousel(options);
			}
		});

		$('.s-our-brands__slider .owl-carousel').owlCarousel({
			items: items,
			loop: true,
			autoplay: 6000
		});
	}

	if($('.s-milestone__slider').length){
		var $slider_container = $('.s-milestone__slider');
		var slider = $slider_container.find(' .owl-carousel').owlCarousel({
				items: 3,
				loop: false,
				responsive: {
					0: {
						items: 1.4
					},
					350: {
						items: 2
					},
					800: {
						items: 3
					}
				}
			});

		$slider_container
			.on('click', '.owl-prev',  function () {
			slider.trigger('prev.owl.carousel');
		})
			.on('click', '.owl-next',  function () {
			slider.trigger('next.owl.carousel');
		});

	}
});
