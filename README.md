# Metropolitan static layout

Using Gulp to automate workflow and Nunjucks for html templating. Babel and Sass supported for faster development.

__Includes:__

- Nunjucks/HTML compilation.
- CSS/Sass processing and minification (autoprefixer, clean-css).
- JavaScript bundling and minification.
- Delivery folder generation - clean and ready for distribution to staging, production, etc.  
- Fully configurable build process.

## Installation

__Step 1:__ Add global packages to your computer.

- [node & npm](https://nodejs.org/)
- [gulp-cli](http://gulpjs.com/)

__Step 2:__ Install dependencies:

```sh
# Install dependancies
$ npm install
```

## Configuration

Default paths and plugin-configurations can be modified to your liking, but anything beyond that may require some Gulp file refactoring.

## File structure

```
app
├── assets // All assets goes here
│   ├── css
│   │   └── *.css
│   ├── fonts
│   ├── images
│   │   └── *.gif, *.jpg, *.png, *.svg
│   ├── js
│   │   └── *.js
│   └── scss
│       └── *.scss
├── pages // Site root. Nunjucks pages to be rended goes here
│   └── *.njk
└── templates // All nunjucks templates and partials goes here
    ├── layouts
    │   └── *.njk
    └── partials
        └── *.njk
```

Do __NOT__ remove or rename the immediate folders within `src` unless you plan to modify the gulp configuration.

## Build scripts

```sh
# Run gulp. Default task complies, runs server and watch for changes
$ gulp
```

```sh
# Run gulp build to build files without serving. 
$ gulp build
```

```sh
# Run gulp watch to watch for changes without serving files. Only compiles when when changes are detected.
$ gulp watch
```

## Notes
- TODO: scss structure needs to be optimized
- TODO: other layouts need to be converted
- TODO: organize static asserts
